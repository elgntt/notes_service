package main

import (
	"github.com/elgntt/notes_service/database"
	"github.com/elgntt/notes_service/handler"
	"log"
	"net/http"
)

func main() {
	log.Println("Server Started")

	mux := http.NewServeMux()
	if err := database.Connect(); err != nil {
		log.Fatal(err)
	}
	defer database.DB.Close()

	mux.HandleFunc("/create", handler.CreateNote)
	mux.HandleFunc("/", handler.ShowNotes)
	mux.HandleFunc("/delete", handler.DeleteNote)
	mux.HandleFunc("/edit", handler.EditNote)

	log.Fatal(http.ListenAndServe(":4040", mux))
}
