package model

type ReceivedNoteText struct { //   The resulting text of the note to add to the database
	Text string `json:"text"`
}

type NotesID struct {
	ID int `json:"id"` //    Received id for deleting an entry in the database
}

type NoteData struct { //     The content of the note
	ID   int    `json:"id"`
	Text string `json:"text"`
}
