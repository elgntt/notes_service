package config

import (
	"fmt"
	"github.com/joho/godotenv"
	"os"
)

// Config
// Getting configuration data from the database_settings.env file
func Config(key string) string {
	err := godotenv.Load("database_settings.env")
	if err != nil {
		fmt.Println("Error loading .env file")
		return ""
	}
	return os.Getenv(key)
}
