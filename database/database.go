package database

import (
	"database/sql"
	"fmt"
	"github.com/elgntt/notes_service/config"
)

var DB *sql.DB

/*
Creating a connection to our database.
Information about the database is stored in the database_settings.env file
*/
func Connect() error {
	var err error

	DB, err = sql.Open("mysql", fmt.Sprintf("%s:%s@/%s", config.Config("DB_USERNAME"), config.Config("DB_PASSWORD"), config.Config("DB_NAME")))
	if err != nil {
		return err
	}
	if err = DB.Ping(); err != nil {
		return err
	}
	CreateNotesTable() // creating a table if there is none
	fmt.Println("Connection Opened to Database")
	return nil
}
