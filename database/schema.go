package database

func CreateNotesTable() {
	DB.Query(`CREATE TABLE IF NOT EXISTS notes(
			id INT PRIMARY KEY AUTO_INCREMENT,
    			note_text VARCHAR(123)
    			)`)
}
