package handler

import (
	"encoding/json"
	"github.com/elgntt/notes_service/database"
	"github.com/elgntt/notes_service/model"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"net/http"
)

// CreateNote
// Creating a note and saving it to the database 
func CreateNote(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8") // normal header
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")

	if r.Method == http.MethodPost { // Processing only POST requests, ignoring any other
		t := new(model.ReceivedNoteText)

		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&t)
		if err != nil {
			log.Println(err)
			return
		}
		if t.Text != "" {
			_, err = database.DB.Exec("insert into notes (note_text) values (?)", t.Text)
			if err != nil {
				log.Println(err)
				return
			}
		}
	}
}

// ShowNotes
// Receiving notes from the baths and then sending them
func ShowNotes(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8") // normal header
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")

	row, err := database.DB.Query("SELECT id, note_text FROM notes;")
	if err != nil {
		log.Println(err)
		return
	}
	defer row.Close()

	var data []*model.NoteData

	for row.Next() {
		s := &model.NoteData{}
		err = row.Scan(&s.ID, &s.Text)
		if err != nil {
			log.Println(err)
			return
		}
		data = append(data, s)
	}
	if err = row.Err(); err != nil {
		log.Println(err)
		return
	}

	jsonData, err := json.Marshal(&data)
	if err != nil {
		log.Println(err)
		return
	}

	w.Write(jsonData)
}

// EditNote
// Getting the changed data and overwriting it into the database
func EditNote(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8") // normal header
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")

	if r.Method == http.MethodPost { // Processing only POST requests, ignoring any other
		t := new(model.NoteData)

		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&t)
		if err != nil {
			log.Println(err)
			return
		}
		_, err = database.DB.Exec("UPDATE notes SET note_text = (?) WHERE id=(?)", t.Text, t.ID)
		if err != nil {
			log.Println(err)
			return
		}
	}
}

// DeleteNote
// Deleting an entry by the received id
func DeleteNote(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8") // normal header
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")

	if r.Method == http.MethodPost { // Processing only POST requests, ignoring any other
		t := new(model.NotesID)

		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&t)
		if err != nil {
			log.Println(err)
			return
		}
		_, err = database.DB.Exec("DELETE FROM notes WHERE id=(?)", t.ID)
		if err != nil {
			log.Println(err)
			return
		}
	}
}
